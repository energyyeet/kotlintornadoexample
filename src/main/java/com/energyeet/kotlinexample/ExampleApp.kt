package com.energyeet.kotlinexample

import com.energyeet.kotlinexample.view.HomeView
import com.energyeet.kotlinexample.view.LogInView
import javafx.scene.Scene
import javafx.stage.Stage
import tornadofx.*
import java.io.File

/**
 * Main Application
 */
class ExampleApp: App(LogInView::class) {

    override fun start(stage: Stage) {
        stage.title = "Kotlin Example App"
        stage.minHeight = 200.0
        stage.minWidth = 400.0;
        super.start(stage)
    }
}

fun main(args: Array<String>) {
    launch<ExampleApp>(args)
}