package com.energyeet.kotlinexample.view

import com.energyeet.kotlinexample.controller.LoginController
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import tornadofx.*

/**
 * Login View
 */
class LogInView: View("Kotlin Example App") {

    val model = ViewModel()
    val username = model.bind { SimpleStringProperty() }
    val password = model.bind { SimpleStringProperty() }

    val controller: LoginController by inject()

    override val root = form {
        fieldset {
            labelPosition = Orientation.VERTICAL
            field("Username") {
                textfield(username)
            }
            field("Password") {
                passwordfield(password)
            }
            button("Log In") {
                style = "-fx-base: #57b757;"
                action {
                    runAsyncWithProgress {
                        controller.login(username.value, password.value)
                    }
                    replaceWith<HomeView>(ViewTransition.Slide(0.3.seconds, ViewTransition.Direction.LEFT))
                }
            }
        }
    }
}