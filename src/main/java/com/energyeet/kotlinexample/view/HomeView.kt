package com.energyeet.kotlinexample.view

import com.energyeet.kotlinexample.controller.HomeController
import com.energyeet.kotlinexample.data.Person
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.TableColumn
import tornadofx.*
import java.time.LocalDate

class HomeView: View("Home View") {

    val username = SimpleStringProperty("")
    val password = SimpleStringProperty("")
    val controller: HomeController by inject()

    val persons = FXCollections.observableArrayList<Person>(
            Person(1,"Danilo", LocalDate.of(2001, 12, 3)),
            Person(2, "Nico", LocalDate.of(2003, 1, 17)),
            Person(3, "Nico", LocalDate.of(2003, 1, 17)),
            Person(4, "Nico", LocalDate.of(2003, 1, 17)),
            Person(5, "Nico", LocalDate.of(2003, 1, 17))
    )

    override val root = tableview(persons) {
                column("Id", Person::id)
                column("Name", Person::name)
                column("Birthday", Person::birthday)
                columnResizePolicy = SmartResize.POLICY
                onDoubleClick {
                    println("Name")
                }
            }
}