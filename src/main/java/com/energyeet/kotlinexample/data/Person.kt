package com.energyeet.kotlinexample.data

import tornadofx.getProperty
import tornadofx.property
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period

class Person(id: Int, name: String, birthday: LocalDate) {
    var id by property<Int>()
    fun idProperty() = getProperty(Person::id)

    var name by property<String>()
    fun nameProperty() = getProperty(Person::name)

    var birthday by property<LocalDate>()
    fun birthdayProperty() = getProperty(Person::birthday)

    //assume today is 2016-02-28
    val age: Int get() = Period.between(birthday, LocalDate.now()).years
    init {
        this.id = id
        this.name = name
        this.birthday = birthday
    }
}