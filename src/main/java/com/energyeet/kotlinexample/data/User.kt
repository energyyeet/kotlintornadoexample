/**
 * User data class
 */
data class User(var username: String, var password: String) {
}