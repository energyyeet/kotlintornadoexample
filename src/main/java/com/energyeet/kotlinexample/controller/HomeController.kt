package com.energyeet.kotlinexample.controller

import javafx.beans.property.SimpleStringProperty
import tornadofx.Controller
import tornadofx.JsonBuilder
import tornadofx.View
import java.io.File
import java.nio.charset.Charset
import javax.json.Json
import javax.json.JsonObject
import kotlin.reflect.KProperty

class HomeController: Controller() {

    fun onEnter(username: SimpleStringProperty, password: SimpleStringProperty) {
        var jsonString = File("./logindata.json").readText(Charset.defaultCharset())
        var jsonBuilder = JsonBuilder()
        println(jsonBuilder)
    }
}