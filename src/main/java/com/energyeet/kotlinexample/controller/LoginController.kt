package com.energyeet.kotlinexample.controller

import User
import com.energyeet.kotlinexample.util.LoginHandler
import javafx.beans.property.SimpleStringProperty
import tornadofx.Controller

/**
 * LoginContrller
 */
class LoginController: Controller() {

    /**
     * Login Action
     */
    fun login(username: String, password: String) {
        val loginHandler: LoginHandler = LoginHandler()
        var user: User = User(username, password)
        loginHandler.handle(user)
    }

}