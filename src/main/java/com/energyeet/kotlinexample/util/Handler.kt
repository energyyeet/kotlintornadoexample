package com.energyeet.kotlinexample.util

/**
 * Interface for handling actions
 * @param T Object parameter to handle
 * @see LoginHandler
 */
interface Handler<T> {

    /**
     * Handle Action
     * @param toHandle Object to handle
     */
    fun handle(toHandle: T)
}