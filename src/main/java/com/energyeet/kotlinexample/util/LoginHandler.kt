package com.energyeet.kotlinexample.util

import User
import com.energyeet.kotlinexample.view.HomeView
import com.energyeet.kotlinexample.view.LogInView
import tornadofx.JsonBuilder
import tornadofx.find
import tornadofx.toPrettyString
import java.io.File

/**
 * Handle the login action
 */
class LoginHandler: Handler<User> {

    override fun handle(toHandle: User) {
        var jsonBuilder = JsonBuilder()

        jsonBuilder.add("username", toHandle.username)
        jsonBuilder.add("password", toHandle.password)
        File("./logindata.json").writeText(jsonBuilder.build().toPrettyString())
    }
}